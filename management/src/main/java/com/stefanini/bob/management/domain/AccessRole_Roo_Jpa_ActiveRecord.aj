// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.stefanini.bob.management.domain;

import com.stefanini.bob.management.domain.AccessRole;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

privileged aspect AccessRole_Roo_Jpa_ActiveRecord {
    
    @PersistenceContext
    transient EntityManager AccessRole.entityManager;
    
    public static final List<String> AccessRole.fieldNames4OrderClauseFilter = java.util.Arrays.asList("code", "description");
    
    public static final EntityManager AccessRole.entityManager() {
        EntityManager em = new AccessRole().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long AccessRole.countAccessRoles() {
        return entityManager().createQuery("SELECT COUNT(o) FROM AccessRole o", Long.class).getSingleResult();
    }
    
    public static List<AccessRole> AccessRole.findAllAccessRoles() {
        return entityManager().createQuery("SELECT o FROM AccessRole o", AccessRole.class).getResultList();
    }
    
    public static List<AccessRole> AccessRole.findAllAccessRoles(String sortFieldName, String sortOrder) {
        String jpaQuery = "SELECT o FROM AccessRole o";
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            jpaQuery = jpaQuery + " ORDER BY " + sortFieldName;
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) {
                jpaQuery = jpaQuery + " " + sortOrder;
            }
        }
        return entityManager().createQuery(jpaQuery, AccessRole.class).getResultList();
    }
    
    public static AccessRole AccessRole.findAccessRole(Long id) {
        if (id == null) return null;
        return entityManager().find(AccessRole.class, id);
    }
    
    public static List<AccessRole> AccessRole.findAccessRoleEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM AccessRole o", AccessRole.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public static List<AccessRole> AccessRole.findAccessRoleEntries(int firstResult, int maxResults, String sortFieldName, String sortOrder) {
        String jpaQuery = "SELECT o FROM AccessRole o";
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            jpaQuery = jpaQuery + " ORDER BY " + sortFieldName;
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) {
                jpaQuery = jpaQuery + " " + sortOrder;
            }
        }
        return entityManager().createQuery(jpaQuery, AccessRole.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    @Transactional
    public void AccessRole.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void AccessRole.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            AccessRole attached = AccessRole.findAccessRole(this.id);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void AccessRole.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void AccessRole.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public AccessRole AccessRole.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        AccessRole merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
}
