package com.stefanini.bob.management.services;
import org.springframework.roo.addon.layers.service.RooService;

@RooService(domainTypes = { com.stefanini.bob.management.domain.PersonAccessRoleRelationship.class })
public interface PersonAccessRoleRelationshipService {
}
