// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.stefanini.bob.management.web;

import com.stefanini.bob.management.domain.PheseGroup;
import com.stefanini.bob.management.services.PheseGroupService;
import com.stefanini.bob.management.web.PheseGroupController;
import java.io.UnsupportedEncodingException;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.util.UriUtils;
import org.springframework.web.util.WebUtils;

privileged aspect PheseGroupController_Roo_Controller {
    
    @Autowired
    PheseGroupService PheseGroupController.pheseGroupService;
    
    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String PheseGroupController.create(@Valid PheseGroup pheseGroup, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, pheseGroup);
            return "phesegroups/create";
        }
        uiModel.asMap().clear();
        pheseGroupService.savePheseGroup(pheseGroup);
        return "redirect:/phesegroups/" + encodeUrlPathSegment(pheseGroup.getId().toString(), httpServletRequest);
    }
    
    @RequestMapping(params = "form", produces = "text/html")
    public String PheseGroupController.createForm(Model uiModel) {
        populateEditForm(uiModel, new PheseGroup());
        return "phesegroups/create";
    }
    
    @RequestMapping(value = "/{id}", produces = "text/html")
    public String PheseGroupController.show(@PathVariable("id") Long id, Model uiModel) {
        uiModel.addAttribute("phesegroup", pheseGroupService.findPheseGroup(id));
        uiModel.addAttribute("itemId", id);
        return "phesegroups/show";
    }
    
    @RequestMapping(produces = "text/html")
    public String PheseGroupController.list(@RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, @RequestParam(value = "sortFieldName", required = false) String sortFieldName, @RequestParam(value = "sortOrder", required = false) String sortOrder, Model uiModel) {
        if (page != null || size != null) {
            int sizeNo = size == null ? 10 : size.intValue();
            final int firstResult = page == null ? 0 : (page.intValue() - 1) * sizeNo;
            uiModel.addAttribute("phesegroups", PheseGroup.findPheseGroupEntries(firstResult, sizeNo, sortFieldName, sortOrder));
            float nrOfPages = (float) pheseGroupService.countAllPheseGroups() / sizeNo;
            uiModel.addAttribute("maxPages", (int) ((nrOfPages > (int) nrOfPages || nrOfPages == 0.0) ? nrOfPages + 1 : nrOfPages));
        } else {
            uiModel.addAttribute("phesegroups", PheseGroup.findAllPheseGroups(sortFieldName, sortOrder));
        }
        return "phesegroups/list";
    }
    
    @RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String PheseGroupController.update(@Valid PheseGroup pheseGroup, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, pheseGroup);
            return "phesegroups/update";
        }
        uiModel.asMap().clear();
        pheseGroupService.updatePheseGroup(pheseGroup);
        return "redirect:/phesegroups/" + encodeUrlPathSegment(pheseGroup.getId().toString(), httpServletRequest);
    }
    
    @RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String PheseGroupController.updateForm(@PathVariable("id") Long id, Model uiModel) {
        populateEditForm(uiModel, pheseGroupService.findPheseGroup(id));
        return "phesegroups/update";
    }
    
    @RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = "text/html")
    public String PheseGroupController.delete(@PathVariable("id") Long id, @RequestParam(value = "page", required = false) Integer page, @RequestParam(value = "size", required = false) Integer size, Model uiModel) {
        PheseGroup pheseGroup = pheseGroupService.findPheseGroup(id);
        pheseGroupService.deletePheseGroup(pheseGroup);
        uiModel.asMap().clear();
        uiModel.addAttribute("page", (page == null) ? "1" : page.toString());
        uiModel.addAttribute("size", (size == null) ? "10" : size.toString());
        return "redirect:/phesegroups";
    }
    
    void PheseGroupController.populateEditForm(Model uiModel, PheseGroup pheseGroup) {
        uiModel.addAttribute("pheseGroup", pheseGroup);
    }
    
    String PheseGroupController.encodeUrlPathSegment(String pathSegment, HttpServletRequest httpServletRequest) {
        String enc = httpServletRequest.getCharacterEncoding();
        if (enc == null) {
            enc = WebUtils.DEFAULT_CHARACTER_ENCODING;
        }
        try {
            pathSegment = UriUtils.encodePathSegment(pathSegment, enc);
        } catch (UnsupportedEncodingException uee) {}
        return pathSegment;
    }
    
}
