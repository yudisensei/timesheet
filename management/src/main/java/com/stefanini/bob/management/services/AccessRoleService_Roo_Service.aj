// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.stefanini.bob.management.services;

import com.stefanini.bob.management.domain.AccessRole;
import com.stefanini.bob.management.services.AccessRoleService;
import java.util.List;

privileged aspect AccessRoleService_Roo_Service {
    
    public abstract long AccessRoleService.countAllAccessRoles();    
    public abstract void AccessRoleService.deleteAccessRole(AccessRole accessRole);    
    public abstract AccessRole AccessRoleService.findAccessRole(Long id);    
    public abstract List<AccessRole> AccessRoleService.findAllAccessRoles();    
    public abstract List<AccessRole> AccessRoleService.findAccessRoleEntries(int firstResult, int maxResults);    
    public abstract void AccessRoleService.saveAccessRole(AccessRole accessRole);    
    public abstract AccessRole AccessRoleService.updateAccessRole(AccessRole accessRole);    
}
