package com.stefanini.bob.management.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Classe utilitária para operações de data e hora
 * @author fpsouza
 */
public class DateTimeUtils {
	/**
	 * Método para acrescentar ou decrescer unidades de uma determinada data retornando a data resultante do cálculo
	 * @param date - data alvo da operação
	 * @param field - inteiro que representa o campo a ser acrescido ou descrescido (Usar o Calendar.DAY_OF_MONTH, por exemplo)
	 * @param amount - quantidade a ser acrescida (passar valor +) ou descrescida (passar valor -)
	 * @return Data acrescida/descrescida da quantidade e unidades passadas como paramêtro. Retorna o valor com horas zeradas.
	 */
	public static Date add(Date date, int field, int amount){
		Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.add(field, amount);
        return calendar.getTime();
	}

	public static Date setToStart(Date date){
		Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 0);
        calendar.set(Calendar.MINUTE, 0);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        return calendar.getTime();
	}

	public static Date setToEnd(Date date){
		Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        calendar.set(Calendar.HOUR_OF_DAY, 23);
        calendar.set(Calendar.MINUTE, 59);
        calendar.set(Calendar.SECOND, 59);
        calendar.set(Calendar.MILLISECOND, 999);
        return calendar.getTime();
	}

	
	public static boolean isMonday(Date date){
		Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return calendar.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY;
	}
	
	public static boolean isWorkDay(Date date){
		if(isWeekend(date)) return true;
		return true;
	}
	
	public static boolean isWeekend(Date date){
		Calendar calendar = new GregorianCalendar();
        calendar.setTime(date);
        return calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY;
	}
	
	public static int getDifferenceBetweenTwoDates(Date date1, Date date2){
		Calendar calendar1 = new GregorianCalendar();
		calendar1.setTime(date1);
		
		Calendar calendar2 = new GregorianCalendar();
		calendar2.setTime(date2);
		
		 long dt1 = calendar1.getTimeInMillis();  
	     long dt2 = calendar2.getTimeInMillis();  
		
		return  (int) (((dt2 - dt1) / 86400000)+1);  
	}
	
	public static Date getOnePastWorkDay(){
		if(isMonday(new Date()))
			return add(new Date(), Calendar.DAY_OF_MONTH, -3);
		return add(new Date(), Calendar.DAY_OF_MONTH, -1);
	}
}
