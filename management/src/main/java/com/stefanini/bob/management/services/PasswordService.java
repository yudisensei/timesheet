package com.stefanini.bob.management.services;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import org.springframework.roo.addon.layers.service.RooService;

import com.stefanini.bob.management.domain.Password;
import com.stefanini.bob.management.domain.Person;

@RooService(domainTypes = { com.stefanini.bob.management.domain.Password.class })
public interface PasswordService {
	
	public Password findOneByPerson(Person person) throws IllegalArgumentException;

	public abstract Password savePassword(Password password, boolean oldKeyPassValidationOff) throws NoSuchAlgorithmException, UnsupportedEncodingException;

	public abstract Password updatePassword(Password password, boolean oldKeyPassValidationOff);

}
