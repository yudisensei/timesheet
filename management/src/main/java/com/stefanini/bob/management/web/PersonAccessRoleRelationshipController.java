package com.stefanini.bob.management.web;
import com.stefanini.bob.management.domain.PersonAccessRoleRelationship;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/personaccessrolerelationships")
@Controller
@RooWebScaffold(path = "personaccessrolerelationships", formBackingObject = PersonAccessRoleRelationship.class)
public class PersonAccessRoleRelationshipController {
}
