package com.stefanini.bob.management.dao;
import com.stefanini.bob.management.domain.PersonAccessRoleRelationship;
import org.springframework.roo.addon.layers.repository.jpa.RooJpaRepository;

@RooJpaRepository(domainType = PersonAccessRoleRelationship.class)
public interface PersonAccessRoleRelationshipDAO {
}
