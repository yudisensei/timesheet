# README #

## Stefanini Timesheet ##

### Resumo ###

Aplicação responsável por receber o registro de atividades de horas distribuída em atividades da equipe

[Produção](http://gestao172.stefanini.com:8088/management)

### Ambiente ###

* [JAVA 7](http://www.oracle.com/technetwork/pt/java/javase/downloads/jdk7-downloads-1880260.html)
* [MAVEN](https://maven.apache.org/)
* [Spring MVC](https://docs.spring.io/spring/docs/current/spring-framework-reference/html/mvc.html)
* [Spring Roo](http://docs.spring.io/spring-roo/docs/1.3.2.RELEASE/reference/html)

### Como contribuir? ###

Todas as necessidades são resgistradas como uma issue no Bitbucket, basta escolher uma e iniciar o desenvolvimento.

### Com quem posso falar sobre o projeto? ###

[Filipe Pacheco Souza](mailto:fpsouza@stefanini.com)