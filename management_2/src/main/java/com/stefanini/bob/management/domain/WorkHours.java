package com.stefanini.bob.management.domain;

import java.math.BigDecimal;

public class WorkHours {
	
	private BigDecimal value;
	
	public WorkHours(BigDecimal value) {
		super();
		this.setValue(value);
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

}
