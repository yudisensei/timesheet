// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.stefanini.bob.management.domain;

import com.stefanini.bob.management.domain.Password;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.transaction.annotation.Transactional;

privileged aspect Password_Roo_Jpa_ActiveRecord {
    
    @PersistenceContext
    transient EntityManager Password.entityManager;
    
    public static final List<String> Password.fieldNames4OrderClauseFilter = java.util.Arrays.asList("keyPass", "person", "oldPassword");
    
    public static final EntityManager Password.entityManager() {
        EntityManager em = new Password().entityManager;
        if (em == null) throw new IllegalStateException("Entity manager has not been injected (is the Spring Aspects JAR configured as an AJC/AJDT aspects library?)");
        return em;
    }
    
    public static long Password.countPasswords() {
        return entityManager().createQuery("SELECT COUNT(o) FROM Password o", Long.class).getSingleResult();
    }
    
    public static List<Password> Password.findAllPasswords() {
        return entityManager().createQuery("SELECT o FROM Password o", Password.class).getResultList();
    }
    
    public static List<Password> Password.findAllPasswords(String sortFieldName, String sortOrder) {
        String jpaQuery = "SELECT o FROM Password o";
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            jpaQuery = jpaQuery + " ORDER BY " + sortFieldName;
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) {
                jpaQuery = jpaQuery + " " + sortOrder;
            }
        }
        return entityManager().createQuery(jpaQuery, Password.class).getResultList();
    }
    
    public static Password Password.findPassword(Long id) {
        if (id == null) return null;
        return entityManager().find(Password.class, id);
    }
    
    public static List<Password> Password.findPasswordEntries(int firstResult, int maxResults) {
        return entityManager().createQuery("SELECT o FROM Password o", Password.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    public static List<Password> Password.findPasswordEntries(int firstResult, int maxResults, String sortFieldName, String sortOrder) {
        String jpaQuery = "SELECT o FROM Password o";
        if (fieldNames4OrderClauseFilter.contains(sortFieldName)) {
            jpaQuery = jpaQuery + " ORDER BY " + sortFieldName;
            if ("ASC".equalsIgnoreCase(sortOrder) || "DESC".equalsIgnoreCase(sortOrder)) {
                jpaQuery = jpaQuery + " " + sortOrder;
            }
        }
        return entityManager().createQuery(jpaQuery, Password.class).setFirstResult(firstResult).setMaxResults(maxResults).getResultList();
    }
    
    @Transactional
    public void Password.persist() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.persist(this);
    }
    
    @Transactional
    public void Password.remove() {
        if (this.entityManager == null) this.entityManager = entityManager();
        if (this.entityManager.contains(this)) {
            this.entityManager.remove(this);
        } else {
            Password attached = Password.findPassword(this.id);
            this.entityManager.remove(attached);
        }
    }
    
    @Transactional
    public void Password.flush() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.flush();
    }
    
    @Transactional
    public void Password.clear() {
        if (this.entityManager == null) this.entityManager = entityManager();
        this.entityManager.clear();
    }
    
    @Transactional
    public Password Password.merge() {
        if (this.entityManager == null) this.entityManager = entityManager();
        Password merged = this.entityManager.merge(this);
        this.entityManager.flush();
        return merged;
    }
    
}
