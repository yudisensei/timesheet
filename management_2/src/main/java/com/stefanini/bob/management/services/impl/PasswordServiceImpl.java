package com.stefanini.bob.management.services.impl;
import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;

import com.stefanini.bob.management.dao.PasswordDAO;
import com.stefanini.bob.management.domain.Password;
import com.stefanini.bob.management.domain.Person;
import com.stefanini.bob.management.services.PasswordService;

public class PasswordServiceImpl implements PasswordService {

	@PersistenceContext
	protected EntityManager entityManager;
	
	@Autowired
	private PasswordDAO passwordDAO;
	
	public Password findOneByPerson(Person person) throws IllegalArgumentException{
		Password password = passwordDAO.findOneByPerson(person);
		
		if(password==null) throw new IllegalArgumentException("Pessoa não possui uma senha cadastrada");
		
		entityManager.detach(password);

		password.setKeyPass("");
		password.setOldPassword("");
		
		return password;
	}
	
	public Password updatePassword(Password password	, boolean oldKeyPassValidationOff) {
		
		String encryptedPass = "";
		
		this.entityManager.detach(password);
		
		Password actualPassword = passwordDAO.findOneByPerson(password.getPerson());
		
		try {
			
			if(actualPassword==null)
				return savePassword(password, oldKeyPassValidationOff);
			
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			
			if(!oldKeyPassValidationOff){
				StringBuffer sb = new StringBuffer();
				byte[] hash = digest.digest(password.getOldPassword().getBytes("UTF-8"));
				for (int i = 0; i < hash.length; i++) {
					sb.append(Integer.toString((hash[i] & 0xff) + 0x100, 16).substring(1));
				}
				password.setOldPassword(sb.toString());
			}
			else
				password.setOldPassword(actualPassword.getKeyPass());
			
			StringBuffer sb = new StringBuffer();
			byte[] hash = digest.digest(password.getKeyPass().getBytes("UTF-8"));
			for (int i = 0; i < hash.length; i++) {
				sb.append(Integer.toString((hash[i] & 0xff) + 0x100, 16).substring(1));
			}
			encryptedPass = sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		}
		
		if(!actualPassword.getKeyPass().equals(password.getOldPassword()) && !oldKeyPassValidationOff)
			throw new IllegalArgumentException("Palavra chave antiga não bate com a fornecida pelo usuário.");
		
		actualPassword.setOldPassword(actualPassword.getKeyPass());
		actualPassword.setKeyPass(encryptedPass);
		
        return passwordDAO.save(actualPassword);
    }

	public List<Password> findAllPasswords() {
        return passwordDAO.findAll();
    }

	public Password findPassword(Long id) {
        return passwordDAO.findOne(id);
    }

	public Password savePassword(Password password, boolean oldKeyPassValidationOff) throws NoSuchAlgorithmException, UnsupportedEncodingException {
		
		Password old = passwordDAO.findOneByPerson(password.getPerson());
		
		if(old!=null){
			old.setKeyPass(password.getKeyPass());
			return updatePassword(old, oldKeyPassValidationOff);
		}else{
			Password newPass = new Password();
			MessageDigest digest = MessageDigest.getInstance("SHA-256");
			password.setOldPassword("");
			StringBuffer sb = new StringBuffer();
			byte[] hash = digest.digest(password.getKeyPass().getBytes("UTF-8"));
			for (int i = 0; i < hash.length; i++) {
				sb.append(Integer.toString((hash[i] & 0xff) + 0x100, 16).substring(1));
			}
			newPass.setKeyPass(sb.toString());
			newPass.setOldPassword(sb.toString());
			newPass.setPerson(password.getPerson());
			newPass.setVersion(1);
			return passwordDAO.save(newPass);
		}
    }
}
