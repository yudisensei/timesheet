package com.stefanini.bob.management.web;
import com.stefanini.bob.management.domain.AccessRole;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/accessroles")
@Controller
@RooWebScaffold(path = "accessroles", formBackingObject = AccessRole.class)
public class AccessRoleController {
}
