package com.stefanini.bob.management.web;
import com.stefanini.bob.management.domain.PheseGroup;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@RequestMapping("/phesegroups")
@Controller
@RooWebScaffold(path = "phesegroups", formBackingObject = PheseGroup.class)
public class PheseGroupController {
}
