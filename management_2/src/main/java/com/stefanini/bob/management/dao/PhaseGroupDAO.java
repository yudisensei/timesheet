package com.stefanini.bob.management.dao;
import com.stefanini.bob.management.domain.PheseGroup;
import org.springframework.roo.addon.layers.repository.jpa.RooJpaRepository;

@RooJpaRepository(domainType = PheseGroup.class)
public interface PhaseGroupDAO {
}
