// WARNING: DO NOT EDIT THIS FILE. THIS FILE IS MANAGED BY SPRING ROO.
// You may push code into the target .java compilation unit if you wish to edit any member(s).

package com.stefanini.bob.management.dao;

import com.stefanini.bob.management.dao.PasswordDAO;
import com.stefanini.bob.management.domain.Password;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.stereotype.Repository;

privileged aspect PasswordDAO_Roo_Jpa_Repository {
    
    declare parents: PasswordDAO extends JpaRepository<Password, Long>;
    
    declare parents: PasswordDAO extends JpaSpecificationExecutor<Password>;
    
    declare @type: PasswordDAO: @Repository;
    
}
