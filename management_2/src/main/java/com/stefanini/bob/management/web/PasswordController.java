package com.stefanini.bob.management.web;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.roo.addon.web.mvc.controller.scaffold.RooWebScaffold;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.stefanini.bob.management.domain.Password;
import com.stefanini.bob.management.domain.Person;
import com.stefanini.bob.management.services.PersonService;

@RequestMapping("/passwords")
@Controller
@RooWebScaffold(path = "passwords", formBackingObject = Password.class)
public class PasswordController {

    private SecurityContextUtils securityContextUtils;

    @Autowired
    private PersonService personService;

    @RequestMapping(value = "/{id}", params = "form", produces = "text/html")
    public String updateForm(@PathVariable("id") Long id, Model uiModel) {
        String returnPage = "passwords/update";
        securityContextUtils = new SecurityContextUtils();
        Password password = new Password();
        if (id == 0) {
            Person person = personService.getPersonByAccessUserName(this.securityContextUtils.getLoggedUserName());
            try {
                password = passwordService.findOneByPerson(person);
            } catch (IllegalArgumentException e) {
                returnPage = "passwords/create";
            }
        }
        populateEditForm(uiModel, password);
        return returnPage;
    }

    void populateEditForm(Model uiModel, Password password) {
        uiModel.addAttribute("password", password);
        uiModel.addAttribute("people", getListOfPeopleByPermission());
    }

    private List<Person> getListOfPeopleByPermission() {
        List<Person> listPeopleToFilter = new ArrayList<Person>();
        securityContextUtils = new SecurityContextUtils();
        Person loggedPerson = personService.getPersonByAccessUserName(securityContextUtils.getLoggedUserName());
        if (securityContextUtils.hasRole("ROLE_ADMIN")) {
            listPeopleToFilter.addAll(personService.findAllPeople());
        } else if (securityContextUtils.hasRole("ROLE_MANAGER")) {
            listPeopleToFilter.addAll(personService.findByManager(loggedPerson));
        } else {
            listPeopleToFilter.add(loggedPerson);
        }
        listPeopleToFilter.remove(loggedPerson);
        listPeopleToFilter.add(0, loggedPerson);
        return listPeopleToFilter;
    }

    @RequestMapping(method = RequestMethod.POST, produces = "text/html")
    public String create(@Valid Password password, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) throws Exception {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, password);
            return "passwords/create";
        }
        uiModel.asMap().clear();
        passwordService.savePassword(password, (securityContextUtils.hasRole("ROLE_ADMIN") || securityContextUtils.hasRole("ROLE_MANAGER")));
        return "redirect:/passwords/" + encodeUrlPathSegment(password.getId().toString(), httpServletRequest);
    }

    @RequestMapping(method = RequestMethod.PUT, produces = "text/html")
    public String update(@Valid Password password, BindingResult bindingResult, Model uiModel, HttpServletRequest httpServletRequest) {
        if (bindingResult.hasErrors()) {
            populateEditForm(uiModel, password);
            return "passwords/update";
        }
        uiModel.asMap().clear();
        Password updated = passwordService.updatePassword(password, (securityContextUtils.hasRole("ROLE_ADMIN") || securityContextUtils.hasRole("ROLE_MANAGER")));
        return "redirect:/passwords/" + encodeUrlPathSegment(updated.getId().toString(), httpServletRequest);
    }
}
