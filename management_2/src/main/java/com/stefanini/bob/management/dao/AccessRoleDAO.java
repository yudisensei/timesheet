package com.stefanini.bob.management.dao;
import com.stefanini.bob.management.domain.AccessRole;
import org.springframework.roo.addon.layers.repository.jpa.RooJpaRepository;

@RooJpaRepository(domainType = AccessRole.class)
public interface AccessRoleDAO {
}
