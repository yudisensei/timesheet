package com.stefanini.bob.management.web;

import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

public class PendingPersonFilter {

	private Long workGroupId;
	
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date filterDataFrom;
	
	@DateTimeFormat(pattern="dd/MM/yyyy")
	private Date filterDataTo;
	
	private Long numberHours;
	
	public PendingPersonFilter(){
		
	}
	
	public PendingPersonFilter(Long workGroupIdParam, Date filterDataFrom, Date filterDataTo, Long numberHours){
		this.workGroupId = workGroupIdParam;
		this.filterDataFrom = filterDataFrom;
		this.filterDataTo = filterDataTo;
		this.numberHours = numberHours;
	}

	/**
	 * @return the workGroupId
	 */
	public Long getWorkGroupId() {
		return workGroupId;
	}

	/**
	 * @param workGroupId the workGroupId to set
	 */
	public void setWorkGroupId(Long workGroupId) {
		this.workGroupId = workGroupId;
	}

	/**
	 * @return the filterDataFrom
	 */
	public Date getFilterDataFrom() {
		return filterDataFrom;
	}

	/**
	 * @param filterDataFrom the filterDataFrom to set
	 */
	public void setFilterDataFrom(Date filterDataFrom) {
		this.filterDataFrom = filterDataFrom;
	}

	/**
	 * @return the filterDataTo
	 */
	public Date getFilterDataTo() {
		return filterDataTo;
	}

	/**
	 * @param filterDataTo the filterDataTo to set
	 */
	public void setFilterDataTo(Date filterDataTo) {
		this.filterDataTo = filterDataTo;
	}

	/**
	 * @return the numberHours
	 */
	public Long getNumberHours() {
		return numberHours;
	}

	/**
	 * @param numberHours the numberHours to set
	 */
	public void setNumberHours(Long numberHours) {
		this.numberHours = numberHours;
	}
}
